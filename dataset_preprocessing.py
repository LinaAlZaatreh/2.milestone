import os
import cv2
import math
import numpy as np


def apply_random_lines(image, num_lines, max_line_length, rotation_degree):
    height, width = image.shape[:2]
    line_counts = {'top': 0, 'bottom': 0, 'left': 0, 'right': 0}

    for _ in range(num_lines):
        line_length = np.random.randint(1, max_line_length)

        available_sides = [side for side, count in line_counts.items() if count == 0]
        if not available_sides:
            break

        side = np.random.choice(available_sides)

        if side == 'top':
            x1 = np.random.randint(0, width)
            y1 = 0
            x2 = np.random.randint(0, width)
            y2 = np.random.randint(0, line_length)
        elif side == 'bottom':
            x1 = np.random.randint(0, width)
            y1 = height - 1
            x2 = np.random.randint(0, width)
            y2 = np.random.randint(height - line_length, height)
        elif side == 'left':
            x1 = 0
            y1 = np.random.randint(0, height)
            x2 = np.random.randint(0, line_length)
            y2 = np.random.randint(0, height)
        elif side == 'right':
            x1 = width - 1
            y1 = np.random.randint(0, height)
            x2 = np.random.randint(width - line_length, width)
            y2 = np.random.randint(0, height)

        color = np.random.randint(0, 256)
        thickness = np.random.randint(1, 2)

        dx = x2 - x1
        dy = y2 - y1
        angle = math.radians(rotation_degree)
        new_dx = int(dx * math.cos(angle) - dy * math.sin(angle))
        new_dy = int(dx * math.sin(angle) + dy * math.cos(angle))
        x2 = x1 + new_dx
        y2 = y1 + new_dy

        cv2.line(image, (x1, y1), (x2, y2), color, thickness)

        line_counts[side] += 1

    return image


def apply_dilation(image, kernel_size=(3, 3), iterations=1):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, kernel_size)
    dilated_image = cv2.dilate(image, kernel, iterations=iterations)
    return dilated_image


def apply_gaussian_noise(image, mean=0, std_dev=10):
    height, width = image.shape
    noise = np.random.normal(mean, std_dev, size=(height, width)).astype(np.float32)
    noisy_image = image + noise
    noisy_image = np.clip(noisy_image, 0, 255).astype(np.uint8)
    return noisy_image


def add_random_points(image, num_points, radius_range=(1, 3), color_range=(0, 256)):
    height, width = image.shape[:2]
    for _ in range(num_points):
        radius = np.random.randint(radius_range[0], radius_range[1])
        x = np.random.randint(0, width)
        y = np.random.randint(0, height)
        color = np.random.randint(color_range[0], color_range[1])
        cv2.circle(image, (x, y), radius, color, -1)
    return image


def apply_noises_to_dataset(images, mean, std_dev, num_points, num_lines, max_line_length, rotation_degree):
    noisy_images = []

    for image in images:
        noisy_image = image.copy()

        noisy_image = apply_gaussian_noise(noisy_image, mean, std_dev)
        noisy_image = apply_random_lines(noisy_image.copy(), num_lines, max_line_length, rotation_degree)
        noisy_image = add_random_points(noisy_image.copy(), num_points)
        noisy_image = apply_dilation(noisy_image.copy(), kernel_size=(3, 3), iterations=1)

        noisy_images.append(noisy_image)

    return noisy_images


def save_images(images, directory):
    os.makedirs(directory, exist_ok=True)
    for i, image in enumerate(images):
        filename = os.path.join(directory, f"{i}.png")
        cv2.imwrite(filename, image)
