import onnx
import tf2onnx
from keras.models import load_model

keras_model = load_model('denoising_model.h5')

onnx_model, _ = tf2onnx.convert.from_keras(keras_model)

onnx.save_model(onnx_model, 'denoising_model.onnx')
