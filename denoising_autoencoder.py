from keras.models import Model
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from dataset_preprocessing import *
from keras.callbacks import EarlyStopping
from skimage.metrics import peak_signal_noise_ratio, mean_squared_error
from keras.layers import Input, Conv2D, MaxPooling2D, UpSampling2D, Dropout

train_images_path = "dataset/train"
val_images_path = "dataset/validation"

image_height = 24
image_width = 200


def load_images_from_directory(directory):
    images = []
    for filename in os.listdir(directory):
        if filename.endswith(".png"):
            image_path = os.path.join(directory, filename)
            image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            image = cv2.resize(image, (image_width, image_height))
            images.append(image)
    return images


def preprocess_images(images):
    processed_images = np.array(images).reshape(-1, image_height, image_width, 1)
    processed_images = processed_images / 255.0
    return processed_images


train_images = load_images_from_directory(train_images_path)
val_images = load_images_from_directory(val_images_path)

mean = 0
std_dev = 10
num_points = 10
num_lines = 5
max_line_length = 20
rotation_degree = 0.2

noisy_train_images = apply_noises_to_dataset(train_images, mean, std_dev, num_points, num_lines, max_line_length,
                                             rotation_degree)
noisy_val_images = apply_noises_to_dataset(val_images, mean, std_dev, num_points, num_lines, max_line_length,
                                           rotation_degree)

print(f"Original Train Images Shape: {train_images[0].shape}")
print(f"Noisy Train Images Shape: {noisy_train_images[0].shape}")

save_images(noisy_train_images, directory="noise_dataset/train")
save_images(noisy_val_images, directory="noise_dataset/validation")

train_images = preprocess_images(train_images)
noisy_train_images = preprocess_images(noisy_train_images)

val_images = preprocess_images(val_images)
noisy_val_images = preprocess_images(noisy_val_images)


train_images = shuffle(train_images, random_state=42)
noisy_train_images = shuffle(noisy_train_images, random_state=42)

val_images = shuffle(val_images, random_state=42)
noisy_val_images = shuffle(noisy_val_images, random_state=42)

input_shape = (image_height, image_width, 1)
inputs = Input(shape=input_shape)

x = Conv2D(128, (3, 3), activation='relu', padding='same')(inputs)
x = MaxPooling2D((2, 2), padding='same')(x)
x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = MaxPooling2D((2, 2), padding='same')(x)

x = Dropout(0.1)(x)

x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
x = UpSampling2D((2, 2))(x)
outputs = Conv2D(1, (3, 3), activation='sigmoid', padding='same')(x)

model = Model(inputs=inputs, outputs=outputs)

model.compile(optimizer='adam', loss='binary_crossentropy')

batch_size = 32
epochs = 15
early_stopping = EarlyStopping(patience=3, restore_best_weights=True)
history = model.fit(noisy_train_images, train_images, batch_size=batch_size, epochs=epochs,
                    validation_data=(noisy_val_images, val_images), callbacks=[early_stopping])

denoised_images = model.predict(noisy_train_images)


def plot_denoised_images(original_images, noisy_images, denoised_images):
    num_images = len(original_images)
    plt.figure(figsize=(15, 4))

    for i in range(num_images):
        plt.subplot(3, num_images, i + 1)
        plt.imshow(original_images[i], cmap='gray')
        plt.title('Original')
        plt.axis('off')

        plt.subplot(3, num_images, num_images + i + 1)
        plt.imshow(noisy_images[i], cmap='gray')
        plt.title('Noisy')
        plt.axis('off')

        plt.subplot(3, num_images, 2 * num_images + i + 1)
        plt.imshow(denoised_images[i], cmap='gray')
        plt.title('Denoised')
        plt.axis('off')

    plt.tight_layout()
    plt.show()


plot_denoised_images(train_images[:5], noisy_train_images[:5], denoised_images[:5])

plt.figure(figsize=(8, 6))
plt.plot(history.history['loss'], label='Loss')
plt.plot(history.history['val_loss'], label='Validation Loss')
plt.title('Training and Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.savefig("loss_plot.png")
plt.show()


def calculate_psnr(original_images, denoised_images):
    psnr_values = []
    for original, denoised in zip(original_images, denoised_images):
        psnr = peak_signal_noise_ratio(original, denoised, data_range=original.max() - original.min())
        psnr_values.append(psnr)
    avg_psnr = np.mean(psnr_values)
    return avg_psnr


def calculate_mse(original_images, denoised_images):
    mse_values = []
    for original, denoised in zip(original_images, denoised_images):
        mse = mean_squared_error(original, denoised)
        mse_values.append(mse)
    avg_mse = np.mean(mse_values)
    return avg_mse


def calculate_mae(original_images, denoised_images):
    mae_values = []
    for original, denoised in zip(original_images, denoised_images):
        mae = np.mean(np.abs(original - denoised))
        mae_values.append(mae)
    avg_mae = np.mean(mae_values)
    return avg_mae


train_psnr = calculate_psnr(train_images, denoised_images)
train_mse = calculate_mse(train_images, denoised_images)
train_mae = calculate_mae(train_images, denoised_images)

print("Training Set Metrics:")
print(f"Average PSNR: {train_psnr} dB")
print(f"Average MSE: {train_mse}")
print(f"Mean Absolute Error (MAE): {train_mae}")

model.save("denoising_model.h5")
