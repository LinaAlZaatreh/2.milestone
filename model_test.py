import os
import matplotlib.pyplot as plt
from keras.models import load_model
from dataset_preprocessing import *
from skimage.metrics import peak_signal_noise_ratio, mean_squared_error

test_images_path = "dataset/test"
image_height = 24
image_width = 200

model = load_model("denoising_model.h5")


def load_images_from_directory(directory):
    images = []
    for filename in os.listdir(directory):
        if filename.endswith(".png"):
            image_path = os.path.join(directory, filename)
            image = cv2.imread(image_path, cv2.IMREAD_GRAYSCALE)
            image = cv2.resize(image, (image_width, image_height))
            images.append(image)
    return images


test_images = load_images_from_directory(test_images_path)

mean = 0
std_dev = 10
num_points = 10
num_lines = 5
max_line_length = 20
rotation_degree = 0.2

noisy_test_images = apply_noises_to_dataset(test_images, mean, std_dev, num_points, num_lines, max_line_length,
                                            rotation_degree)

save_images(noisy_test_images, directory="noise_dataset/test")

test_images = np.array(test_images).reshape(-1, image_height, image_width, 1)
noisy_test_images = np.array(noisy_test_images).reshape(-1, image_height, image_width, 1)

test_images = test_images / 255.0
noisy_test_images = noisy_test_images / 255.0


denoised_images = model.predict(noisy_test_images)


def calculate_psnr(original_images, denoised_images):
    psnr_values = []
    for original, denoised in zip(original_images, denoised_images):
        psnr = peak_signal_noise_ratio(original, denoised, data_range=original.max() - original.min())
        psnr_values.append(psnr)
    avg_psnr = np.mean(psnr_values)
    return avg_psnr


def calculate_mse(original_images, denoised_images):
    mse_values = []
    for original, denoised in zip(original_images, denoised_images):
        mse = mean_squared_error(original, denoised)
        mse_values.append(mse)
    avg_mse = np.mean(mse_values)
    return avg_mse


def calculate_mae(original_images, denoised_images):
    mae_values = []
    for original, denoised in zip(original_images, denoised_images):
        mae = np.mean(np.abs(original - denoised))
        mae_values.append(mae)
    avg_mae = np.mean(mae_values)
    return avg_mae


def plot_denoised_images(original_images, noisy_images, denoised_images):
    num_images = len(original_images)
    plt.figure(figsize=(15, 4))

    for i in range(num_images):
        plt.subplot(3, num_images, i + 1)
        plt.imshow(original_images[i], cmap='gray')
        plt.title('Original')
        plt.axis('off')

        plt.subplot(3, num_images, num_images + i + 1)
        plt.imshow(noisy_images[i], cmap='gray')
        plt.title('Noisy')
        plt.axis('off')

        plt.subplot(3, num_images, 2 * num_images + i + 1)
        plt.imshow(denoised_images[i], cmap='gray')
        plt.title('Denoised')
        plt.axis('off')

    plt.tight_layout()
    plt.show()


plot_denoised_images(test_images[:5], noisy_test_images[:5], denoised_images[:5])

loss = model.evaluate(noisy_test_images, test_images, verbose=0)
print(f"Loss: {loss}")

psnr = calculate_psnr(test_images, denoised_images)
mse = calculate_mse(test_images, denoised_images)
mae = calculate_mae(test_images, denoised_images)

print(f"PSNR: {psnr} dB")
print(f"MSE: {mse}")
print(f"MAE: {mae}")
