import os
import cv2
import numpy as np
from keras.datasets import mnist
from sklearn.utils import shuffle

(train_images, _), (test_images, _) = mnist.load_data()


def extract_digits(image):
    _, binary_image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)

    blurred_image = cv2.GaussianBlur(binary_image, (5, 5), 0)

    contours, _ = cv2.findContours(blurred_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    digit_images = []
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        digit_image = image[y:y + h, x:x + w]
        digit_images.append(digit_image)

    return digit_images


def resize_digits(digit_images, target_width, target_height):
    resized_images = []
    for image in digit_images:
        resized_image = cv2.resize(image, (target_width, target_height))
        resized_images.append(resized_image)
    return resized_images


def generate_multi_digit_strings(images, string_length, intersection_amount, max_distance):
    num_samples = images.shape[0]
    digit_strings = []

    for _ in range(num_samples):
        indices = np.random.choice(num_samples, string_length, replace=True)
        selected_images = images[indices]

        digit_images = []
        last_x = 0
        last_width = 0

        for image in selected_images:
            extracted_digits = extract_digits(image)
            if not extracted_digits:
                digit_images = []
                break
            digit_images.extend(extracted_digits)

        if not digit_images:
            continue

        max_height = max(image.shape[0] for image in digit_images)
        digit_images = resize_digits(digit_images, target_width=20, target_height=max_height)

        final_image = np.zeros((digit_images[0].shape[0], 0), dtype=np.uint8)

        for digit_image in digit_images:
            intersection = int(intersection_amount * digit_image.shape[1])
            max_dist = int(max_distance * digit_image.shape[1])

            random_x = np.random.randint(last_x + last_width - intersection, last_x + last_width + max_dist)

            final_image = np.hstack((final_image, digit_image))

            last_x = random_x
            last_width = digit_image.shape[1]

        digit_strings.append(final_image)

    return digit_strings


def save_images(images, directory):
    os.makedirs(directory, exist_ok=True)
    for i, image in enumerate(images):
        filename = os.path.join(directory, f"{i}.png")
        cv2.imwrite(filename, image)


def split_dataset(images, train_ratio, test_ratio, val_ratio):
    images = shuffle(images, random_state=42)

    total_samples = len(images)
    train_samples = int(total_samples * train_ratio)
    test_samples = int(total_samples * test_ratio)
    val_samples = int(total_samples * val_ratio)

    train_images = images[:train_samples]
    test_images = images[train_samples:train_samples + test_samples]
    val_images = images[train_samples + test_samples:train_samples + test_samples + val_samples]

    return train_images, test_images, val_images


string_length = 10
intersection_amount = 0.4
max_distance = 0.02

multi_digit_strings = generate_multi_digit_strings(train_images, string_length, intersection_amount, max_distance)

train_images, test_images, val_images = split_dataset(multi_digit_strings, train_ratio=0.7, test_ratio=0.2,
                                                      val_ratio=0.1)

save_images(train_images, directory="dataset/train")
save_images(test_images, directory="dataset/test")
save_images(val_images, directory="dataset/validation")
