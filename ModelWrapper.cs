using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;

namespace Wrapper
{
    public class ModelWrapper
    {
        public static InferenceSession Denoising;
        
        public static void Main(string[] args)
        {
            string directory = Directory.GetCurrentDirectory();
            string inputImagePath = Path.Combine(directory, "18.png");
            
            Bitmap inputImage = new Bitmap(inputImagePath);

            string modelPath = Path.Combine(directory, "denoising_model.onnx");
            
            Denoising = new InferenceSession(modelPath);

            float[] normalizedImage = NormalizeImage(inputImage);

            float[] array = DenoiseImage(normalizedImage);

            Bitmap denoisedImage = CreateGrayscaleBitmapFromArray(array);

            denoisedImage.Save("denoised_image.bmp", ImageFormat.Bmp);
            Console.WriteLine("Image Denoised Successfully");
            
            inputImage.Dispose();
            denoisedImage.Dispose();
        }

        public static float[] NormalizeImage(Bitmap image)
        {
            int width = image.Width;
            int height = image.Height;

            BitmapData imageData = image.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
            
            float[] normalizedImage = new float[width * height];

            unsafe
            {
                int stride = imageData.Stride;
                byte* imageScan0 = (byte*)imageData.Scan0;

                for (int y = 0; y < height; y++)
                {
                    byte* pointer = imageScan0 + (y * stride);

                    for (int x = 0; x < width; x++)
                    {
                        int index = y * width + x;
                        normalizedImage[index] = pointer[x] / 255.0f;
                    }
                }
            }

            image.UnlockBits(imageData);
            return normalizedImage;
        }
        
        public static float[] DenoiseImage(float[] image)
        {
            var inputLayer = Denoising.InputMetadata.Keys.First();

            int[] inputShape = { 1, 24, 200, 1 };
            var inputTensor = new DenseTensor<float>(image, inputShape);
            var inputData = new List<NamedOnnxValue>
            {
                NamedOnnxValue.CreateFromTensor(inputLayer, inputTensor)
            };

            var output = Denoising.Run(inputData);
            var outputTensor = (DenseTensor<float>)output.First().Value;
            return outputTensor.ToArray();
        }

        public static Bitmap CreateGrayscaleBitmapFromArray(float[] array)
        {
            int width = 200;
            int height = 24;

            Bitmap denoisedImage = new Bitmap(width, height, PixelFormat.Format8bppIndexed);

            BitmapData denoisedImageData = denoisedImage.LockBits(new Rectangle(0, 0, width, height),
                ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);

            ColorPalette palette = denoisedImage.Palette;
            for (int i = 0; i < 256; i++)
            {
                palette.Entries[i] = Color.FromArgb(i, i, i);
            }
            denoisedImage.Palette = palette;

            unsafe
            {
                byte* scan0 = (byte*)denoisedImageData.Scan0;
                int stride = denoisedImageData.Stride;
                int imageSize = width * height;
                int arrayIndex = 0;

                for (int y = 0; y < height; y++)
                {
                    byte* pointer = scan0 + (y * stride);

                    for (int x = 0; x < width; x++)
                    {
                        if (arrayIndex < imageSize)
                        {
                            byte pixelValue = (byte)(array[arrayIndex] * 255);
                            pointer[x] = pixelValue;
                            arrayIndex++;
                        }
                    }
                }
            }

            denoisedImage.UnlockBits(denoisedImageData);
            return denoisedImage;
        }
    }
}
